<?php

use src\Clipper;
use src\Uploader;

require_once dirname(dirname(__FILE__)).DIRECTORY_SEPARATOR.'bootstrap.php';

$uploadDir = UPLOAD_DIR.DIRECTORY_SEPARATOR;

if(isset($_FILES['files'])){

    $uploader = new Uploader();
    $data = $uploader->upload($_FILES['files'], array(
        'limit' => 1, //Maximum Limit of files. {null, Number}
        'maxSize' => 100, //Maximum Size of files {null, Number(in MB's)}
        'extensions' => null, //Whitelist for file extension. {null, Array(ex: array('jpg', 'png'))}
        'required' => false, //Minimum one file is required for upload {Boolean}
        'uploadDir' => $uploadDir, //Upload directory {String}
        'title' => array('name'), //New file name {null, String, Array} *please read documentation in README.md
        'removeFiles' => true, //Enable file exclusion {Boolean(extra for jQuery.filer), String($_POST field name containing json data with file names)}
        'replace' => true, //Replace the file if it already exists  {Boolean}
        'perms' => null, //Uploaded file permisions {null, Number}
        'onCheck' => null, //A callback function name to be called by checking a file for errors (must return an array) | ($file) | Callback
        'onError' => null, //A callback function name to be called if an error occured (must return an array) | ($errors, $file) | Callback
        'onSuccess' => null, //A callback function name to be called if all files were successfully uploaded | ($files, $metas) | Callback
        'onUpload' => null, //A callback function name to be called if all files were successfully uploaded (must return an array) | ($file) | Callback
        'onComplete' => null, //A callback function name to be called when upload is complete | ($file) | Callback
        'onRemove' => null //A callback function name to be called by removing files (must return an array) | ($removed_files) | Callback
    ));

    if($data['isComplete']){
        $info = $data['data'];

        $uploadInfo = [];
        foreach ($info['metas'] as $meta){
            $type = 'video';
            $uploadInfo[$type]['file'] = $meta['file'];
            $uploadInfo[$type]['extension'] = $meta['extension'];

        }

        if(!empty($uploadInfo)){
            //$xmlPath = CONFIG_DIR.DIRECTORY_SEPARATOR.'video_clips.xml'; // not being used currently
            $videoPath = $uploadInfo['video']['file'];
            $videoExtension = $uploadInfo['video']['extension'];

            $clipper = new Clipper();

            $clipper->makeClips($videoPath, $videoExtension);

            header('Location: preview.php');
            exit;

        }else{
            header('Location: index.php');
            exit;
        }

    }

}
