<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Video Clipper</title>

    <!-- Google Fonts -->
    <link href="https://fonts.googleapis.com/css?family=Roboto+Condensed" rel="stylesheet">

    <!-- Bootstrap -->
    <link href="resources/bower_components/bootstrap/dist/css/bootstrap.min.css" rel="stylesheet">

    <!-- jQuery Filer -->
    <link href="resources/bower_components/jquery.filer/css/jquery.filer.css" rel="stylesheet">

    <!-- Custom -->
    <link href="resources/css/custom.css" rel="stylesheet">

</head>
<body>
<div class="container">
    <div class="jumbotron">
        <h1>Video Clipper</h1>
        <p class="lead">
            You can upload MP4 video. The application will create clips of video according to predefined configuration.
        </p>
        <p>
            <form action="process.php" method="post" enctype="multipart/form-data">
                <input type="file" name="files[]" id="filer_input" multiple="multiple">
                <input type="submit" value="Upload" class="btn btn-default">
            </form>
        </p>
    </div>
</div>


<!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
<script src="resources/bower_components/jquery/dist/jquery.min.js"></script>
<!-- Include all compiled plugins (below), or include individual files as needed -->
<script src="resources/bower_components/bootstrap/dist/js/bootstrap.min.js"></script>
<script src="resources/bower_components/jquery.filer/js/jquery.filer.min.js" type="text/javascript"></script>
<script src="resources/js/custom.js" type="text/javascript"></script>
</body>
</html>
