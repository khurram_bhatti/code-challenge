/**
 * Created by Khalid A on 11/14/2016.
 */

$(document).ready(function(){

    $('#filer_input').filer({
        limit: 1,
        maxSize: 1000,
        extensions: ["mp4", "xml"],
        showThumbs: true,
        addMore: false,
        allowDuplicates: false
    });

});

