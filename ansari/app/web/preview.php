<?php
require_once dirname(dirname(__FILE__)).DIRECTORY_SEPARATOR.'bootstrap.php';

if(!isset($_SESSION['main_video_file_name'])){
    header('Location: index.php');
    exit;
}
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Video Clipper</title>

    <!-- Google Fonts -->
    <link href="https://fonts.googleapis.com/css?family=Roboto+Condensed" rel="stylesheet">

    <!-- Bootstrap -->
    <link href="resources/bower_components/bootstrap/dist/css/bootstrap.min.css" rel="stylesheet">

    <!-- Custom -->
    <link href="resources/css/custom.css" rel="stylesheet">

</head>
<body>
<div class="container">
    <div class="jumbotron">
        <h1>Video Clipping Preview</h1>
        <p class="lead">Original Video</p>

            <div class="row">
                <div class="col-xs-12">
                    <video style="width: 100%" controls>
                        <source src="../uploads/<?php echo $_SESSION['main_video_file_name']; ?>" type="video/mp4">
                        Your browser does not support the video tag.
                    </video>
                </div>
            </div>
            <p class="lead">Video Clips</p>
            <div class="row">
                <?php foreach ($_SESSION['clip_file_name'] as $clipName):?>
                <div class="col-sm-4">
                    <video style="width: 100%" controls>
                        <source src="../uploads/<?php echo $clipName; ?>" type="video/mp4">
                        Your browser does not support the video tag.
                    </video>
                </div>
                <?php endforeach;?>
            </div>

    </div>
</div>


<!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
<script src="resources/bower_components/jquery/dist/jquery.min.js"></script>
<!-- Include all compiled plugins (below), or include individual files as needed -->
<script src="resources/bower_components/bootstrap/dist/js/bootstrap.min.js"></script>
<!-- Custom -->
<script src="resources/js/custom.js" type="text/javascript"></script>
<?php unset($_SESSION['main_video_file_name'], $_SESSION['clip_file_name']);?>
</body>
</html>