Video Clipper
-------------
A basic PHP based application to make clip of video files with FFmpeg.

This application requires a working FFMpeg install. Be sure that the binary can be located with system PATH.

Deployment
----------
As it is an PHP based application it can be deployed on PHP supported webservers like Apache, nginx.

Usage
-----
The index file is located under "VideoClipper/app/web/".
File upload and clips are created under "VideoClipper/app/uploads/".