<?php
namespace src;

/**
 * Handle video clipping using FFmpeg library.
 * Class Clipper
 * @package src
 */

class Clipper
{
    /**
     * Demonstration data for video clipping
     * @var array
     */
    private $clipsConfig = [
        ['name' => 'Clip_1', 'start_time' => '00:00:00.00', 'end_time' => '00:00:30.00'],
        ['name' => 'Clip_2', 'start_time' => '00:01:05.00', 'end_time' => '00:01:12.00'],
        ['name' => 'Clip_3', 'start_time' => '00:02:00.00', 'end_time' => '00:02:59.00'],
    ];


    /**
     * Create video clips using FFmpeg according to config data.
     * @param $videoPath
     * @param $extension
     */
    public function makeClips($videoPath, $extension)
    {

        $_SESSION['main_video_file_name'] = basename($videoPath);

        foreach ($this->clipsConfig as $vClip){

            $clipFile = dirname($videoPath).DIRECTORY_SEPARATOR.$vClip['name'].'.'.$extension;
            $startTime = $vClip['start_time'];
            $endTime = $vClip['end_time'];

            $cmd = "ffmpeg -i {$videoPath} -ss {$startTime} -to {$endTime}  -acodec copy -async 1 -y {$clipFile}"; //  start time and end time

            $_SESSION['clip_file_name'][] = basename($clipFile);

            $output = shell_exec($cmd);

        }


    }

}