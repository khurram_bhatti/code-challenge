<?php
/**
 * Created by PhpStorm.
 * User: Khalid A
 * Date: 11/14/2016
 * Time: 7:20 PM
 */
$ds = DIRECTORY_SEPARATOR;
define('APP_DIR', dirname(dirname(__FILE__)));
define('CONFIG_DIR', APP_DIR.$ds.'config');
define('UPLOAD_DIR', APP_DIR.$ds.'uploads');
