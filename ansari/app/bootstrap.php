<?php
/**
 * Created by PhpStorm.
 * User: Khalid A
 * Date: 11/14/2016
 * Time: 7:17 PM
 */
session_start();
$configfile = dirname(__FILE__).DIRECTORY_SEPARATOR.'config'.DIRECTORY_SEPARATOR.'app.php';
if (!file_exists($configfile)) {
    throw new RuntimeException('Configuration Loading Fails!');
}

require_once $configfile;


$autolaodfile = dirname(__FILE__).DIRECTORY_SEPARATOR.'AutoLoader.php';
if (!file_exists($autolaodfile)) {
    throw new RuntimeException('Auto Loading Fails!');
}

require_once $autolaodfile;