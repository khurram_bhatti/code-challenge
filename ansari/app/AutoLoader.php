<?php
/**
 * Class AutoLoader
 */
class AutoLoader
{
    /**
     * Auto load classes
     * @param $className
     * @return bool
     */
    static public function loader($className) {
        $filename = APP_DIR . DIRECTORY_SEPARATOR . str_replace('\\', DIRECTORY_SEPARATOR , $className) . ".php";

        if (file_exists($filename)) {
            include($filename);
            if (class_exists($className)) {
                return TRUE;
            }
        }
        return FALSE;
    }

}
spl_autoload_register('Autoloader::loader');