<?php
/**
 * MyFFMpeg class for generate video clips
 *
 * PHP version 5
 *
 * @author     Bahadur O. <bahadur.o@allshoreresources.com>
 * @copyright  2016 Allshoreresources
 * @version   0.1
 */

class MyFFMpeg
{

    private $ffmpeg;            // command ffmpeg library

    private $options;           // option array from user

    private $cmd;               // other options on ffmpeg command

    public $command;            // hold command ready to execute

    /**
     * myFFMpeg constructor.
     * @param string $ffmpeg
     */
    function __construct($ffmpeg = 'ffmpeg')
    {

        $this->ffmpeg = $ffmpeg;
    }

    /**
     * Generate complete ffmpeg command based on options provided
     * @access protected
     */
    protected function generateFFMpegCommand()
    {

        $this->command = $this->ffmpeg . " " . implode(' ', $this->cmd);
    }

    /**
     * @param array $options
     * @access public
     */
    public function input($options)
    {

        $this->options = $options;
        if (file_exists($options['input_file']) AND is_file($options['input_file'])) {
            $this->setInputFile();
        } else {
            trigger_error("File " . $options['input_file'] . " doesn't exist", E_USER_ERROR);
        }
    }

    /**
     * Generating ffmpeg command with input file
     * @access protected
     */
    protected function setInputFile()
    {

        // setting up ffmpeg initial command
        $this->ffmpeg = $this->ffmpeg . " -i " . $this->options['input_file'];
    }

    /**
     * Preparing other options for ffmpeg command
     * @param string $key
     * @param string $value
     * @access private
     */
    private function set($key, $value)
    {
        // prepare ffmpeg command as key value
        $this->cmd[] = "-$key $value";
    }

    /**
     * Clear options on ffmpeg command
     * @access private
     */
    private function clear()
    {
        // set cmd command to null
        $this->cmd = null;
    }

    /**
     * Prepare and Execute ffmpeg command
     * @access public
     */
    public function execute()
    {

        foreach ($this->options['clips'] as $option) {

            // Clear previous ffmpeg command if exists
            $this->clear();
            // Preparing start and end time to video clip on ffmpeg command
            $this->set('ss', $option['start_time']);
            $this->set('to', $option['duration']);

            switch($option['format']){

                case 'mp4':
                    // Preparing output options in ffmpeg command for mp4 video clip
                    $this->set('acodec copy -async 1 -y', $option['dir'] . $option['output_file'] . '.' . $option['format']);
                    break;
            }

            // Generate ffmpeg command
            $this->generateFFMpegCommand();

            // Executing ffmpeg command using php shell_exec function
            shell_exec($this->command);
        }
    }
}

