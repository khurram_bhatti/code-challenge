MyFFMpeg CLASS
(written by Bahadur O.  2016 )
------------------------------------

.:: INTRODUCTION ::.

With help of this class, you can create video chunks from video provided to that class.
---------------------------------------------------------------------------------------------

.:: DEPENDENCIES ::.

    - shell functions should be enabled in php.ini.
    - ffmpeg library should be installed on server.
    - Video file should be less than 2MB. Otherwise, adjust upload_max_filesize and post_max_size in your php.ini.

.:: HOW TO INSTALL ::.

    Simply extract the files from zip, and place into your localhost root.

.:: HOW TO RUN ::.

    Open url from browser (if your localhost is diffrent from 'localhost', please replace with your IP ). eg: http://<localhost>/myffmpeg
