<?php
/**
 * User: Bahadur O
 * Date: 11/7/2016
 * Time: 10:28 PM
 */
require_once "video_upload_script.php";
?>
<!doctype html>
<html>
    <meta charset="utf-8">
    <head>
        <script
            src="https://code.jquery.com/jquery-3.1.1.min.js"
            integrity="sha256-hVVnYaiADRTO2PzUGmuLJr8BLUSjGIZsDYGmIJLv2b8="
            crossorigin="anonymous"></script>
        <title>Generate video clips</title>
    </head>
    <body>
        <?php if($err) : ?>
        <div style="color: red;"><?php echo $msg?></div>
            <a href="index.php" >Back..</a>
        <?php  else :  ?>
            Video Clips Generated:<br>
            <?php foreach($ffmpegOptions['clips'] as $video_clip) : ?>
                <?php echo $dir.$video_clip['output_file'].'.'.$video_clip['format']?><br>
            <?php endforeach; ?>
            <a href="index.php" >Back..</a>
        <?php endif; ?>
    </body>
</html>