<?php
/**
 * Configurations for process ffmpeg
 *
 * PHP version 5
 *
 * @author     Bahadur O. <bahadur.o@allshoreresources.com>
 * @copyright  2016 Allshoreresources
 * @version   0.1
 */

$upload_max_filesize = (int) ini_get('upload_max_filesize');

// Check if shell_exec function enabled
if(!function_exists('shell_exec')) {
    $msg = "Shell_exec not enabled.";
    $err = true;

}
// Check if ffmpeg is installed
$testFFMpeg = trim(shell_exec('which ffmpeg'));
if(empty($testFFMpeg)){
    $msg = "ffmpeg is not installed.";
    $err = true;
}

